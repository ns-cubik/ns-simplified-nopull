import React from "react";

class SimpleFormThreeForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = { email: '' };
    }
  
    handleChange = (event) => {
      this.setState({[event.target.email]: event.target.value});
    }
  
    handleSubmit = (event) => {
      alert('A form was submitted: ' + this.state);
  
      fetch('https://your-node-server-here.com/api/endpoint', {
          method: 'POST',
          // We convert the React state to JSON and send it as the POST body
          body: JSON.stringify(this.state)
        }).then(function(response) {
          console.log(response)
          return response.json();
        });
  
      event.preventDefault();
  }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
          <label>
            Email:
            <input type="email" value={this.state.value} name="email" onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      );
    }
  }
export default SimpleFormThreeForm