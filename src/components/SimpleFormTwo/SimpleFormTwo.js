import React, { useState } from "react";
//const COLORS = ["white", "red", "blue", "black", "cream"];

function SimpleFormTwoForm() {
  const [values, setValues] = useState({
    email: ""
  });

  const set = (name) => {
    return ({ target: { value } }) => {
      setValues((oldValues) => ({ ...oldValues, [name]: value }));
    };
  };

  const saveFormData = async () => {
    const response = await fetch("/api/registration", {
      method: "POST",
      body: JSON.stringify(values)
    });
    if (response.status !== 200) {
      throw new Error(`Request failed: ${response.status}`);
    }
  };

  const onSubmit = async (event) => {
    event.preventDefault(); // Prevent default submission
    try {
      await saveFormData();
      alert("Your password re-send was successfully submitted!");
      setValues({
        email: ""
      });
    } catch (e) {
      alert(`Registration failed! ${e.message}`);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <h2>Re-Send you password</h2>

      <label>Email*:</label>
      <input
        type="email"
        required
        value={values.email}
        onChange={set("email")}
      />

      <button type="submit">Submit</button>
    </form>
  );
}

export default function Page() {
  return (
    <div className="App">
      <SimpleFormTwoForm />
    </div>
  );
}
