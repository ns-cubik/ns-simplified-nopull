import React, { useReducer, useState, useEffect } from 'react';
//import './components/ForgotPassword/ForgotPassword.scss';

//import './../SignIn/SignIn.scss';
//import './../ResetPasswordDialog/ResetPasswordDialog.scss';

//import ResetPasswordDialog from "../ResetPasswordDialog/ResetPasswordDialog";

//import './components/ForgotPassword/ForgotPassword.scss';

// get our fontawesome imports
import { faUser} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


class FormSubmitApi extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: '' };
  }

  handleChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit = (event) => {
    alert('A form was submitted: ' + this.state);



     // PUT request using fetch with set headers
     const requestOptions = {
        method: 'PATCH',
        headers: { 
            'Content-Type': 'application/json',
            'Authorization': '861a7dbd482a3e9dd0cba0eed0b602a3',
            'api-username': 'shawnp',
            'My-Custom-Header': 'send member his pw'
        },
        body: JSON.stringify({ title: 'Send Member his PW' })
    };
    fetch('https://nakedswordcash.com/api/member/details', {
        method: 'POST',
        // We convert the React state to JSON and send it as the POST body
        body: JSON.stringify(this.state)
      }).then(function(response) {
        console.log(response)
        return response.json();
      });

    event.preventDefault();
}

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" value={this.state.value} name="name" onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
export default FormSubmitApi;