import React, { useState, useEffect } from 'react';
import { faUser} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function PutRequestSetHeaders() {
    const [postId, setPostId] = useState(null);

    useEffect(() => {
        // PUT request using fetch with set headers
        const requestOptions = {
          method: 'PATCH',
          headers: { 
              'Content-Type': 'application/json',
              'Authorization': '861a7dbd482a3e9dd0cba0eed0b602a3',
              'api-username': 'shawnp'
            },
            body: JSON.stringify({ title: 'React Hooks PUT Request Example' })
        };
        fetch('https://nakedswordcash.com/api/member/details', requestOptions)
            .then(response => response.json())
            .then(data => setPostId(data.id));
    }, []);

    return (
        <div className="card text-center m-3">
            <h5 className="card-header">PUT Request with Set Headers</h5>
            <div className="card-body">
                Returned Id: {postId}
            </div>
        </div>
    );
}

export { PutRequestSetHeaders };











import React, { useReducer, useState, useEffect } from 'react';
//import './components/ForgotPassword/ForgotPassword.scss';

//import './../SignIn/SignIn.scss';
//import './../ResetPasswordDialog/ResetPasswordDialog.scss';

//import ResetPasswordDialog from "../ResetPasswordDialog/ResetPasswordDialog";

//import './components/ForgotPassword/ForgotPassword.scss';

// get our fontawesome imports
import { faUser} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


class FormSubmitApi extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: '' };
  }

  handleChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit = (event) => {
    alert('A form was submitted: ' + this.state);



     // PUT request using fetch with set headers
     const requestOptions = {
        method: 'PATCH',
        headers: { 
            'Content-Type': 'application/json',
            'Authorization': '861a7dbd482a3e9dd0cba0eed0b602a3',
            'api-username': 'shawnp'
        },
        body: JSON.stringify({ title: 'Send Member his PW' })
    };
    fetch('https://nakedswordcash.com/api/member/details', {
        method: 'PATCH',
        // We convert the React state to JSON and send it as the POST body
        body: JSON.stringify(this.state)
      }).then(function(response) {
        console.log(response)
        return response.json();
      });

    event.preventDefault();
}

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Email:
          <input type="text" value={this.state.value} name="email" onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit Email" />
      </form>
    );
  }
}
export default FormSubmitApi;