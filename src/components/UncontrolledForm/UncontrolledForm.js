class UncontrolledForm extends React.Component {
  constructor() {
    this.nameInput = React.createRef();
  }

  submitFormHandler = (event) => {
    event.preventDefault();
    console.log(this.nameInput.current.value);
  };

  render() {
    return (
      <form onSubmit={this.submitFormHandler}>
        <div>
          <input type="text" name="name" ref={this.nameInput} />
        </div>
      </form>
    );
  }
}