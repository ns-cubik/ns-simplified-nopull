import './fonts/yanonekaffeesatz-regular.ttf';
import './fonts/yanonekaffeesatz-regular.woff2';
import './fonts/yanonekaffeesatz-regular.woff';
import './fonts/yanonekaffeesatz-bold.ttf';
import './fonts/yanonekaffeesatz-bold.woff';
import './fonts/yanonekaffeesatz-bold.woff2';
import './fonts/Metropolis-Regular.eot';
import './fonts/Metropolis-Regular.woff2';
import './fonts/Metropolis-Regular.woff';
import './fonts/Metropolis-Regular.ttf';
import './fonts/Metropolis-Black.eot';
import './fonts/Metropolis-Black.woff2';
import './fonts/Metropolis-Black.woff';
import './fonts/Metropolis-Black.ttf';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
